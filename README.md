# PETS Exercise

## General Remarks

- The notebooks can be started via the terminal: jupyter notebook PETS_ExerciseX.ipynb
- The exercises are described below. You can find the TODOs within the respective .py files.


## Exercise 1) Symmetric Encryption

- Step 1: Install the cryptography package for python: pip install cryptography
- Step 2: Open the notebook PETS_Exercise1.ipynb and identify the TODOs
- Step 3: You should not need any additional imports, however, you can import whatever you like to finish the tasks
***
- Hint 1: The goal is that a file is encrypted via a key that is generated through a password. The key is the salted hash of the password entered by the user.
- Hint 2: You do not have to save the password nor the salt to disk. It is sufficient if it works during one session.


## Exercise 4_1) k-anonymity

- Step 1
- Step 2

## Exercise 4_2) Differential Privacy

- Step 1
- Step 2

## Exercise 5) Machine Learning

- Step 1
- Step 2

## Exercise 8) Partial Homomorphic Encryption (ElGamal)

- Step 1
- Step 2